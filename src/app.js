export default class GitSearch {

    constructor () {
        this.OnCreate();
    }

    // Method for link creator
    // Set user search references
    // If search don't return references, function set default parameters
    getUrl (srsearch) {

        let q = srsearch || 'es6',
            per_page = 5;

        const link = "https://api.github.com/search/repositories?";

        return `${link}q=${q}&per_page=${per_page}`;

    };

    // Method for data resolve
    getData (q) {

        new Promise(() => {
            return fetch(this.getUrl(q))
                .then(response => {
                // Convert data to JSON
                return response.json();
                })
                .then(data => {
                        // Call method for data
                        this.prepareData(data.items)
                })
                .catch(() => {
                        // If rejection from server , throw an error
                        console.log('can`t get data from server');
                });
        });
    }

    // Method for data sort
    prepareData (data) {

        // Create array for data collect
        this.data = [];

        // With for of - loop data object
        // Sort params and push in to array
        for (let value of data) {

            this.data.push ({
                title: value.name,
                pageId: value.id,
                snippet: value.description,
                url: value.html_url
            });
        }

        this.Render();

    }

    OnCreate () {
        this.addEvents();
    };

    // Check if after new search old data is deleted
    DeleteResults () {
        let resultContent = document.getElementById("result-content");
        if (resultContent) {
            myApp.removeChild(resultContent);
        }
    }

    // Method for data preview
    Render () {

        let dataArray = this.data,
            myApp = document.getElementById("myApp");

        this.DeleteResults();

        // Check if data exist and is array, if not, throw error
        if (dataArray.length && Array.isArray(dataArray)) {

            // Create global element for results
            let globalContent = document.createElement("article");
            globalContent.id = "result-content";
            myApp.appendChild(globalContent);

            // With loop append all objects into global container
            for (let object of dataArray) {

                let resultContent = document.createElement("section");
                resultContent.className = "result";
                globalContent.appendChild(resultContent);

                let title = document.createElement("a");
                title.href = object.url;
                title.setAttribute('target', '_blank');
                title.innerHTML = object.title;
                resultContent.appendChild(title);

                let description = document.createElement("p");
                description.innerHTML = object.snippet;
                resultContent.appendChild(description);

            }

        } else {
            console.log("this data can't display");
        }

    }

    // Method for events
    // Works if user search by click button or press enter
    addEvents () {

        let self = this;
        let input = document.getElementById("search-input");
        let button = document.getElementById("submit-button");

        input.addEventListener('keypress', function (e) {

            if (13 === e.keyCode) {
                console.log(input.value);
                self.getData(input.value);
            }
        });

        button.addEventListener('click', function () {
            self.getData(input.value);
        });

    }
}

new GitSearch();
